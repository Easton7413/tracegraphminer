#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/basic/List.h>
#include <sstream>
using namespace ogdf;
using namespace std;

#ifndef _TRACE_GRAPH_H
#define _TRACE_GRAPH_H


// Keeps track of timestamp at which the edge was taken
class TS {
	double m_ts;	// timestamp
	edge m_Edge; 	// The host edge
	TS* m_NextTS;	// The next timestamp
public:
	TS(double ts, edge e) {
		m_ts = ts;
		m_Edge = e;
		m_NextTS = 0;
	}

	void Pr() { cout << "t" << m_ts << " "; }
	double ts() { return m_ts; }
	void nextTS(TS* ts) { m_NextTS = ts; }
	TS* nextTS() { return m_NextTS; }
	edge theEdge() { return m_Edge; }
	edge nextEdge() { 
		return (m_NextTS ? m_NextTS->theEdge() : 0);
	}
};


class MaximalPattern;

// Graph-representation of a trace
class TraceGraph : public Graph {
	EdgeArray<List<TS*> > m_labelList;
	GraphAttributes m_GA;
	int m_threshold;
	
	EdgeArray<MaximalPattern*> m_MaxPatterns;

public:
	TraceGraph(const char* kevAsCsv, int th = 1000);
	~TraceGraph();
	
	void clear(int level);

	void readTraceCsv(const char* fName);
	List<TS*>& getTimeStamp(edge e) { 
		return (m_labelList[e]);
	}
	GraphAttributes& attributes() { 
		return m_GA;
	}

	int threshold() {
		return m_threshold;
	}

	void threshold(int th) {
		m_threshold = th;
	}

	void hidefrequentEdges(int freq) {
		List<edge> toHide;
		edge e;
		forall_edges(e, *this) {
			if (getTimeStamp(e).size() > freq)
				toHide.pushBack(e);
		}

		for (edge e : toHide)
			hideEdge(e);
	}
	
	void hideInfrequentEdges() {
		List<edge> toHide;
		edge e;
		forall_edges(e, *this) {
			if (getTimeStamp(e).size() < m_threshold)
				toHide.pushBack(e);
		}

		for (edge e : toHide)
			hideEdge(e);
	}


	string label(node n) {
		return m_GA.label(n);
	}

	TS* getFirstTS() {
		edge e = firstEdge();
		return getTimeStamp(e).front();
	}

	string printEdge(edge e, bool labelsOn = true) {
		stringstream ss;
		if (labelsOn)
			ss << label(e->source()) << " -- " << label(e->target()) << " ";
		else 
			ss << e;
		return ss.str();
	}

	void Pr() {
		edge e;
		forall_edges(e, *this) {
			cout << printEdge(e) << "(";
			for (TS* ts : getTimeStamp(e))
				ts->Pr();
			cout << ")" << endl;
		}
	}

	void eliminateDuplicates();
	bool includes(const MaximalPattern* p1, const MaximalPattern* p2);
	void createPathSubgraphs(const char* prefix);
	void createPatternSubgraphs(const char* prefix);
	void combineFrequentPaths();
	void findAllFrequentPaths();
	void printMaximalPatterns();
	
	void writeDGF(const char* fName);
	
	void writeTrace(const char* fName);
	void printStatus(const char* prefix);

};

#endif
