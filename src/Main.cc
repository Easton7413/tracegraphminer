#include "TraceGraph.h"
#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/planarity/BoyerMyrvold.h>
using namespace ogdf;
using namespace std;


void degreeInfo(Graph& G)
{
	int maxDegree = 0;
	node v;
	forall_nodes(v, G)
		maxDegree = max(v->degree(), maxDegree);
	cout << "Max degree " << maxDegree << endl;
	BoyerMyrvold P;
	cout << "Planner " << P.isPlanar(G) << endl;
}


int main(int argc, char* argv[]) {
    if (argc < 2) {
		cout << "Usage: " << argv[0] << " trace-edges.txt" << endl;
		cout << "For example: " << argv[0] << " ./tests/traces/hexacopter-small.txt threshold-freq[optional]" << endl;
		return 0;
	}
	
	int th = 1000;
	if (argc == 3)
		th = atoi(argv[2]);
	TraceGraph TG(argv[1], th);
	// TG.Pr();
	
	degreeInfo(TG);
	//TG.printStatus("Before hiding frequent Edges");
	//TG.hidefrequentEdges(th);
	
	TG.printStatus("Before hiding infrequent Edges");
	TG.hideInfrequentEdges();
	TG.printStatus("Before searching patterns");

	TG.findAllFrequentPaths();
	TG.createPathSubgraphs("generated-patterns");
	/*TG.combineFrequentPaths();
	TG.printStatus("after hiding non-pattern edges");
	degreeInfo(TG);

	TG.createPatternSubgraphs("generated-patterns");*/
}


