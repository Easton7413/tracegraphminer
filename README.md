# README #

A tool to represent system-wide traces as graphs and extract frequent patterns using graph mining.

Uses OGDF ([http://ogdf.net/doku.php]) for graphs and basic graph algorithms.  

