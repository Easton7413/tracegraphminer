#include <iostream>
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/MinPriorityQueue.h>
#include <ogdf/fileformats/GraphIO.h>
using namespace ogdf;

namespace SubgraphIso {

bool searchEdge(Graph& G, node v, node w) {
	edge e = G.searchEdge(v, w);
	if (e && v == e->source())
		return true;
	return false;
}

bool checkMapping(Graph& H, Graph& G, NodeArray<node>& assignments)
{
	// For all edges with assigned endpoints in pattern H, there must be
	// corresponding edge in host G
	edge e;
	forall_edges(e, H) {
		node u = e->source();
		node v = e->target();
		if (assignments[u] && assignments[v]) {
			if (!searchEdge(G, assignments[u], assignments[v]))
				return false;
		}
	}
	return true;
}

void prune(Graph& H, Graph& G, NodeArray<List<node> >& candidates)
{
	bool change = true;
	while (change) {
		change = false;
		node v;
		forall_nodes(v, H) {
			edge e;
			forall_adj_edges(e, v) {
				node w = e->opposite(v);
				for (node m : candidates[v]) {
					bool mapFound = false;
					edge p;
					forall_adj_edges(p, m) {
						node n = p->opposite(m);
						for (node wCand : candidates[w]) {
							if (wCand == n) {
								mapFound = true;
								break;
							}
						}
						if (mapFound == false) {
							// Remove m from candidates[v]
							change = true;
							candidates[v].removeFirst(m);
						}
					}
				}
			}
		}
	}
}

bool searchSubgraph(Graph& H, Graph& G, 
					NodeArray<List<node> >& candidates, 
					NodeArray<node>& assignments,
					Array<node>& vertices,
					int curIdx)
{
	if (!checkMapping(H, G, assignments))
		return false;
	
	if (curIdx == H.numberOfNodes())
		return true;

	//prune(H, G, candidates);
	node curr = vertices[curIdx];
	List<node>& currCandidates = candidates[curr];

	for (node n : currCandidates) {
		bool assigned = false;
		for (int i = 0; i <= curIdx; i++)
			if (assignments[vertices[i]] == n)
				assigned = true;
		
		if (assigned)
			continue;
		
		assignments[curr] = n;
		if (searchSubgraph(H, G, candidates, assignments, vertices, curIdx + 1))
			return true;
		assignments[curr] = 0;
	}
	return false;
}		

void orderByDegree(Array<node>& vertices, Graph& H)
{
	MinPriorityQueue<int, node> pq(H.numberOfNodes());
    NodeArray<const HeapElement<int, node>*> heapElHandle(H);
    node v;
    forall_nodes(v, H) {
        HeapElement<int, node> he(-1 * v->degree(), v);
        heapElHandle[v] = pq.insert(he);
    }
	
	int idx = 0;
	while (!pq.empty())
		vertices[idx++] = pq.pop().element();
}

void initCandidates(NodeArray<List<node> >& candidates, Graph& H, Graph& G)
{
	node v;
	forall_nodes(v, H) {
		node n;
		forall_nodes(n, G) {
			if (v->indeg() <= n->indeg() && v->outdeg() <= n->outdeg())
				candidates[v].pushBack(n);
		}
	}
}

bool findSubgraph(Graph& G, Graph& H, bool printMappings = false)
{
	NodeArray<List<node> > candidates(H);
	initCandidates(candidates, H, G);

	NodeArray<node> assignments(H, 0);
	
	Array<node> vertices(H.numberOfNodes());
	orderByDegree(vertices, H);

	bool rVal = searchSubgraph(H, G, candidates, assignments, vertices, 0);

	// Print found mappings
	if (rVal && printMappings) {
		cout << "Mapping : " << endl;
		node v;
		forall_nodes(v, H)
			cout << "n" << v << " -> " << "n" << assignments[v] << endl;
	}
	else
		cout << "No Mapping found" << endl;
	
	return rVal;
}

} // end namespace SubgraphIso


// Unit test
/*
int main(int argc, char* argv[])
{
    if (argc != 3) {
		cout << "Usage: " << argv[0] << " pattern.dot graph.dot" << endl;
		return 0;
	}
	
	Graph H; // Pattern
	GraphIO::readDOT(H, argv[1]);
	Graph G; // Graph
	GraphIO::readDOT(G, argv[2]);
	SubgraphIso::findSubgraph(G, H, true);
}
*/
