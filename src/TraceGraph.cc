#include <iostream>
#include <map>
#include <string>
#include <sstream>
#include <ogdf/basic/simple_graph_alg.h>
#include <ogdf/basic/NodeSet.h>
#include "TraceGraph.h"
#include "MaximalPattern.h"

using namespace std;

TraceGraph::TraceGraph(const char* kevAsCsv, int th) : 
			m_labelList(*this),
			m_MaxPatterns(*this, 0),
			m_GA(*this, GraphAttributes::nodeLabel | GraphAttributes::edgeLabel)
{
	cout << "Reading Input" << endl;
    readTraceCsv(kevAsCsv);
    m_threshold = th;

    edge e;
    forall_edges(e, *this)
		m_GA.label(e) = to_string(getTimeStamp(e).size());
}

void TraceGraph::createPathSubgraphs(const char* prefix)
{
	// Check sanity
	edge e;
	forall_edges(e, *this)
		if (m_MaxPatterns[e])
			OGDF_ASSERT(e == m_MaxPatterns[e]->hostEdge());

	int pathIdx = 0;
	for(MaximalPattern* mp : m_MaxPatterns) {
		if (!mp)
			continue;
		NodeSet ns(*this);
		for (edge e : mp->edges()) {
			ns.insert(e->source());
			ns.insert(e->target());
		}
		PatternGraph pg(ns.nodes(), mp->edges(), *this);
		stringstream ss;
		ss << prefix << "/path_" << pathIdx++ << ".dot";
		pg.writeGraph(ss.str().c_str());
	}
	cout << "Created " << pathIdx << " maximal patterns" << endl;
}

void TraceGraph::createPatternSubgraphs(const char* prefix)
{
	NodeArray<int> compId(*this);
	int num = connectedComponents(*this, compId);
	
	Array<List<node> > comp2Nodes(num);
	node n;
	forall_nodes(n, *this)
		comp2Nodes[compId[n]].pushBack(n);

	int compIdx = 0;
	for (List<node>& nodes : comp2Nodes) {
		if (nodes.size() > 2) { // somewhat interesting
			PatternGraph pg(nodes, *this);
			stringstream ss;
			ss << prefix << "/pattern_" << compIdx++ << ".dot";
			pg.writeGraph(ss.str().c_str());
		}
	}
}

void TraceGraph::printStatus(const char* prefix)
{
	NodeArray<int> components(*this);
	int num = connectedComponents(*this, components);
	cout << prefix << " : ";
	cout << numberOfNodes() << " nodes, " << numberOfEdges() << " edges ";
	cout << "and " << num << " connected components" << endl;
}

void TraceGraph::combineFrequentPaths()
{
	EdgeArray<bool> combined(*this, false);
	for(MaximalPattern* mp : m_MaxPatterns) {
		if (!mp)
			continue;
		if (mp->edges().size() < 5)
			continue;
		for (edge e : mp->edges())
			combined[e] = true;
	}

	List<edge> toHide;
	edge e;
	forall_edges(e, *this)
		if (!combined[e])
			toHide.pushBack(e);
	for (edge e : toHide)
		hideEdge(e);
}

void TraceGraph::findAllFrequentPaths()
{
	edge e;
	// do it for all the edges caching paths (memoization)
	forall_edges(e, *this) {
		MaximalPattern* mp =  new MaximalPattern(e, *this);
		m_MaxPatterns[e] = mp;
		mp->growDFS(e->target(), getTimeStamp(e));
		mp->sort();
	}
	eliminateDuplicates();
}

void TraceGraph::printMaximalPatterns()
{
	for (MaximalPattern* mp : m_MaxPatterns)
		mp->Pr();
}

// Checks if p2 is included in p1
bool TraceGraph::includes(const MaximalPattern* p1, const MaximalPattern* p2)
{
	const List<edge>& p1Edges = p1->edges();
	const List<edge>& p2Edges = p1->edges();
	// Edge lists are already sorted. We will try to locate the
	// smaller list in the bigger one
	ListConstIterator<edge> pe = p1Edges.search(p2Edges.front());
	if (!pe.valid())
		return false;
	ListConstIterator<edge> pe2 = p2Edges.search(p2Edges.front());
	while (*pe == *pe2) {
		pe = pe.succ();
		pe2 = pe2.succ();
		if (!pe.valid() || !pe2.valid())
			break;
	}
	return (pe == pe2); // both should be null pointer iterators
}

void TraceGraph::eliminateDuplicates()
{
	int count = 0;
	cout << "Eliminating redundant patterns : ";
	Array<const MaximalPattern*> patterns(this->numberOfEdges());
	int idx = 0;
	for (MaximalPattern* mp : m_MaxPatterns)
		patterns[idx++] = mp;
	OGDF_ASSERT(idx == this->numberOfEdges());

	// Sort patterns by size (low to high)
	patterns.quicksort();
	for (idx = 1; idx < patterns.size(); idx++) {
		const MaximalPattern* mp = patterns[idx];
		if (mp->size() == 1) {
			// You are important, but not actually a pattern
			delete m_MaxPatterns[mp->hostEdge()];
			m_MaxPatterns[mp->hostEdge()] = 0;
		}
		for (edge e : mp->edges()) {
			// Compare with a pattern starting at this edge as a merge candidate
			MaximalPattern* mCandidate = m_MaxPatterns[e];
			if (mCandidate && mCandidate->size() < mp->size()) {
				if (includes(mp, mCandidate)) {
					delete m_MaxPatterns[e];
					m_MaxPatterns[e] = 0;
					count++;
				}
			}
		}
	}

	cout << count << endl;
}

// Cleanup
TraceGraph::~TraceGraph()
{
	restoreAllEdges();
	for (MaximalPattern* mp : m_MaxPatterns)
		delete mp;
	edge e;
    forall_edges(e, *this) {
        for (TS* ts : m_labelList[e])
            delete ts;
    }
}

// Read a trace and convert to a graph
void TraceGraph::readTraceCsv(const char* fName)
{
	map<string, node> nodeMap;
    ifstream dgf(fName);
	if (!dgf.is_open()) {
		cout << "Can't open file" << endl;
		return;
	}
    string curr;
	getline(dgf, curr);
	string prev = curr;

	edge ePrev = 0;
    while (getline(dgf, curr)) {
    	// create an edge between prev and current lines
		double labelSrc, labelDst;
		string src, dst;
		stringstream ss;
		ss << prev << ' ' << curr;
		ss >> labelSrc >> src >> labelDst >> dst;

		if (nodeMap.find(src) == nodeMap.end()) {
			node n =  newNode();
			m_GA.label(n) = src;
			nodeMap.insert(pair<string, node>(src, n));
		}
		if (nodeMap.find(dst) == nodeMap.end()) {
			node n =  newNode();
			m_GA.label(n) = dst;
			nodeMap.insert(pair<string, node>(dst, n));
		}

		node u = nodeMap.find(src)->second;
		node v = nodeMap.find(dst)->second;

		edge uv = searchEdge(u, v);
		if (!uv)
			uv = newEdge(u, v);

		TS* currTS = new TS(labelSrc, uv);
		m_labelList[uv].pushBack(currTS);
		if (ePrev)
			m_labelList[ePrev].back()->nextTS(currTS);
		ePrev = uv;

		prev = curr;
	}
}

// Should give back the original trace
void TraceGraph::writeTrace(const char* fName)
{
	ofstream dgf(fName);
	if (!dgf.is_open()) {
		cout << "Can't open file" << endl;
		return;
	}

	TS* ts = getFirstTS();
	while (ts) {
		edge e = ts->theEdge();
		OGDF_ASSERT(e);
		ts->Pr();
		cout << label(e->source()) << endl;
		ts = ts->nextTS();
	}
}
 
// Obsolete (Keep as may need it in future)
void TraceGraph::writeDGF(const char* fName)
{
    ofstream dgf(fName);
	if (!dgf.is_open()) {
		cout << "Can't open file" << endl;
		return;
	}

	node v;
	forall_nodes(v, *this)
		dgf << "v " << (v->index() + 1) << " " << m_GA.label(v) << endl;
	
	edge e;
	forall_edges(e, *this)
		dgf << "d " << (e->source()->index() + 1) << " " << (e->target()->index() + 1) << " edge" << endl;	
}

