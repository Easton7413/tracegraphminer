#!/usr/bin/perl -w

if ($#ARGV != 1)
{
	print "Generates an edgelist from a kev trace given in csv format\n";
	print "Usage $0 filename.csv integer-abstraction-level[event=1|process=2|thread=3]\n";
	exit 1;
}

use constant {
	EVENT   => 1,
	PNAME 	=> 2,
	TID		=> 3,
};
$level = $ARGV[1];

sub parseLine {
	@line = split(',', $_[0]);
	$time = eval($line[1]);
	$class = eval($line[3]);
	$event = eval($line[4]);
	#$pid = eval($line[5]);
	$tid = eval($line[6]);
	$pname = "";
	if ($line[8] =~ m/.*\/(.*)"$/)
	{
		$pname = $1;
	}
	chomp $pname;
	if ($tid =~ m/NA/)
	{
		$tid = "";
	}
	
	$nodeText = "$class";
	if ($level >= EVENT && $event)
	{
		$nodeText .= "_$event";
	}

	if ($level >= PNAME && $pname)
	{
		$nodeText .= "_$pname";
	}

	if ($level >= TID && $tid)
	{
		$nodeText .= "_$tid";
	}

	$fullText = "$class"."_$event"."_$pname"."_$tid";
	return "$time,$fullText,$nodeText";

}

%abstracted = ();
%abstractCount = ();

open(CSV, "<$ARGV[0]") or die "Can't open $ARGV[0]\n";
while(<CSV>)
{
	($time, $fullText, $nodeText) = split(",", parseLine($_));
	
	if (!exists $abstracted{$fullText})
	{
		$abstracted{$fullText} = $nodeText;
		if (exists $abstractCount{$nodeText})
		{
			$abstractCount{$nodeText} += 1;
		}
		else
		{
			$abstractCount{$nodeText} = 1;
		}
	}
}

seek(CSV, 0, 0);

while(<CSV>)
{
	($time, $fullText, $nodeText) = split(",", parseLine($_));
	print "$time $nodeText";
	if ($abstractCount{$nodeText} > 1)
	{
		print "#($abstractCount{$nodeText})";
	}
	print "\n";
}

close(CSV);
